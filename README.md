# Video to MP3 converter

Inside k8s this program uses RabbitMQ, MySQL, MongoDB and Python.

## Deployment

See [Docs](./docs/)

## LICENSE

See [LICENSE](./LICENSE)
