---
services:
  auth:
    build: src/auth/
    container_name: auth
    entrypoint: sh
    command: docker-entrypoint.sh
    depends_on:
      - auth_db
    volumes:
      - py3_cache:/app/.cache
      - apk_cache:/etc/apk/cache
      - ./src/auth/:/app/
    environment:
      - FLASK_RUN_PORT=${AUTH_PORT}
    restart: unless-stopped
    env_file:
      - src/auth/.env.auth
      - src/auth/.env.auth_db
    networks:
      - auth
      - auth-db

  auth_db:
    build:
      context: src/auth/
      dockerfile: Dockerfile.db
    container_name: auth_db
    volumes:
      - auth_db_data:/var/lib/mysql
      - ./src/auth/db/init.sql:/docker-entrypoint-initdb.d/init.sql
    environment:
      - MYSQL_RANDOM_ROOT_PASSWORD=yes
    restart: unless-stopped
    env_file: src/auth/.env.auth_db
    networks:
      - auth-db

  router:
    build: src/router/
    container_name: router
    ports:
      - ${ROUTER_PORT}:${ROUTER_PORT}
    entrypoint: sh
    command: docker-entrypoint.sh
    depends_on:
      - auth
      - queue
      - redis
      - mongoDB
    volumes:
      - py3_cache:/app/.cache
      - apk_cache:/etc/apk/cache
      - ./src/router/:/app/
    environment:
      - FLASK_RUN_PORT=${ROUTER_PORT}
      - AUTH_PORT=${AUTH_PORT}
    restart: unless-stopped
    env_file:
      - src/router/.env.router
      - src/mongo/.env.mongo
      - src/queue/.env.queue
    networks:
      - router
      - mongo-db
      - redis
      - queue
      - auth

  redis:
    image: redis:alpine
    container_name: redis
    restart: unless-stopped
    volumes:
      - redis_data:/data
    networks:
      - redis

  mongoDB:
    image: mongo
    container_name: mongoDB
    volumes:
      - mongo_data:/data/db
    restart: unless-stopped
    networks:
      - mongo-db

  queue:
    image: rabbitmq:3-management-alpine
    container_name: queue
    hostname: queue
    ports:
      - ${QUEUE_PORT}:${QUEUE_PORT}
    volumes:
      - queue_cache:/var/lib/rabbitmq
      - ./src/queue/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf
    restart: unless-stopped
    env_file: src/queue/.env.queue
    networks:
      - queue

  mail:
    build: src/mail/
    container_name: mail
    entrypoint: sh
    command: docker-entrypoint.sh
    depends_on:
      - queue
      - mongoDB
    volumes:
      - py3_cache:/app/.cache
      - apk_cache:/etc/apk/cache
      - ./src/mail/:/app/
    restart: unless-stopped
    env_file:
      - src/mail/.env.mail
      - src/queue/.env.queue
      - src/mongo/.env.mongo
    networks:
      - mail
      - mongo-db
      - queue

  resolver:
    build: src/resolver/
    container_name: resolver
    entrypoint: sh
    command: docker-entrypoint.sh
    depends_on:
      - queue
      - mongoDB
    volumes:
      - py3_cache:/app/.cache
      - apk_cache:/etc/apk/cache
      - ./src/resolver/:/app/
    restart: unless-stopped
    env_file:
      - src/queue/.env.queue
      - src/mongo/.env.mongo
    networks:
      - resolver
      - mongo-db
      - queue

networks:
  auth:
  auth-db:
  redis:
  router:
  mongo-db:
  queue:
  mail:
  resolver:

volumes:
  py3_cache:
  apk_cache:
  auth_db_data:
  redis_data:
  mongo_data:
  queue_cache:
