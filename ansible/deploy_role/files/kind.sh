#!/usr/bin/env bash


# For AMD64 / x86_64
[ $(uname -m) = x86_64 ] && curl -#Lo /tmp/kind https://kind.sigs.k8s.io/dl/latest/kind-linux-amd64

# For ARM64
[ $(uname -m) = aarch64 ] && curl -#Lo /tmp/kind https://kind.sigs.k8s.io/dl/latest/kind-linux-arm64
