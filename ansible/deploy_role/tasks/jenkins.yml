---
# tasks executed in jenkins hosts

- name: Download stable Jenkins release repo
  get_url:
    url: https://pkg.jenkins.io/redhat-stable/jenkins.repo
    dest: /etc/yum.repos.d/jenkins.repo
    mode: 0755

- name: Import Jenkins key
  rpm_key:
    key: https://pkg.jenkins.io/redhat-stable/jenkins.io-2023.key
    state: present

- name: Install JDK + Jenkins packages
  dnf:
    name: "{{ item }}"
    state: latest
  loop:
    - java-17-openjdk-devel
    - jenkins

- name: Add jenkins to docker group
  user:
    name: jenkins
    state: present
    create_home: false
    groups:
      - docker
    append: true

- name: Start and enable Jenkins service
  systemd:
    name: jenkins
    state: started
    enabled: true
    daemon_reload: true

- name: Allow Jenkins to run sudo commands password-less
  lineinfile:
    path: /etc/sudoers
    state: present
    line: "jenkins ALL=(ALL) NOPASSWD: ALL"
    validate: /usr/sbin/visudo -cf %s

- name: 3 seconds pause, to allow Jenkins to start
  wait_for:
    timeout: 3
    port: 8080

- name: Print Jenkins' URL
  debug:
    msg: "Please visit http://{{ inventory_hostname }}:8080/login"

- name: Reveal Jenkins generated password
  shell: cat /var/lib/jenkins/secrets/initialAdminPassword
