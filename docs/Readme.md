# Rational

I wanted to start tinkering with k8s, so I have decided to create this MP3
converter.

![design](./design.svg)

## Components

### Auth

This is the Authentication flask server. It generates and validates JWT tokens.

Has your typical endpoints:

- home
- login
- logout
- register
- validate

Connects to:

- MariaDB (auth_db)

### Router

This is the "front-end", therefore our users connect exclusively to this
component.

It's nothing but a small flask server. With session/cookies support.

Has your typical endpoints:

- home
- login
- logout
- register
- upload
- download

Connects to:

- RabbitMQ (queue)
- Mongo (mongoDB)
- redis
- auth

### Mail

[ethereal](https://ethereal.email/) was chosen as smtp mail service. It is free
and easy to use.

This component is a simple python script responsible of acknowledging MP3 queue
messages and send notification e-mail.

Connects to:

- RabbitMQ (queue)
- Mongo (mongoDB)

### (Message) Receiver

This component is a simple python script responsible of acknowledging Video
queue messages and converting them to MP3.

Connects to:

- RabbitMQ (queue)
- Mongo (mongoDB)

### (RabbitMQ) Queue

Hearbeats were disabled in the configuration, because application would stop
working.

After default 60s timeout. More details
[here](https://rabbitmq.com/heartbeats.html)
