# Deployment

## Deploy with docker-compose

```bash
# with latest docker engine
# sudo apt/yum install docker-compose-plugin

# (default) configuration
# docker-compose
cp .env.default .env


# auth
cp src/auth/{.env.default,.env.auth}
cp src/auth/{.env.default,.env.auth_db}


# mail
cp src/mail/{.env.default,.env.mail}


# mongo
cp src/mongo/{.env.default,.env.mongo}


# queue
cp src/queue/{.env.default,.env.queue}


# router
cp src/router/{.env.default,.env.router}


docker compose up
```

## Deploy with k8s

```bash
# assuming you use minikube
docker-compose build

# load the image inside of k8s cluster
for i in {auth{,_db},router,mail,resolver}; do
    minikube image load video_converter-${i}
done

# remove previous deployment, for clean start (optional)
for i in `ls src/`; do
    kubectl delete -f src/${i}/k8s; sleep 3
done

# deploys into k8s - assumed namespace "default"
for i in `ls src/`; do
    kubectl apply -f src/${i}/k8s; sleep 3
done
```
