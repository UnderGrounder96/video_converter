#!/usr/bin/env groovy

// File/Dirs
src = "src"

src_mongo_k8s = "${src}/mongo/k8s/"
src_queue_k8s = "${src}/queue/k8s/"
src_redis_k8s = "${src}/redis/k8s/"

src_auth = "${src}/auth"
src_auth_k8s = "${src_auth}/k8s/"

src_router = "${src}/router"
src_router_k8s = "${src_router}/k8s/"

src_mail = "${src}/mail"
src_mail_k8s = "${src_mail}/k8s/"

src_resolver = "${src}/resolver"
src_resolver_k8s = "${src_resolver}/k8s/"


// Dockerfiles
df_auth_db = "${src_auth}/Dockerfile.db"


// Image names
img_auth = "video_converter-auth"
img_auth_db = "video_converter-auth_db"

img_mail = "video_converter-mail"
img_router = "video_converter-router"
img_resolver = "video_converter-resolver"



return this
