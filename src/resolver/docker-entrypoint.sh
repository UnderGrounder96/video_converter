#!/bin/sh

echo "Waiting for RabbitMQ to start..."
sh wait-for.sh ${RABBITMQ_DEFAULT_HOST}:${RABBITMQ_DEFAULT_PORT}

echo "Starting the resolver consumer..."
python3 converter.py
