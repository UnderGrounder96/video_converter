#!/usr/bin/env python3

import config
import os
import sys
import json
import pika
import tempfile

from gridfs import GridFS
from subprocess import run
from bson.objectid import ObjectId
from utils import queue


def converter(channel, message, fs_videos, fs_mp3s):
    message = json.loads(message)

    mp3_file = f"{tempfile.gettempdir()}/{message['video_fid']}.mp3"

    with tempfile.NamedTemporaryFile() as video_file:
        try:
            # video contents
            out = fs_videos.get(ObjectId(message['video_fid']))

            # add video contents to empty file
            video_file.write(out.read())

            run(
                f"ffmpeg -y -i {video_file.name} {mp3_file}",
                shell=True,
                check=True,
                capture_output=True)
        except (Exception) as err:
            return err

    with open(mp3_file, "rb") as data:
        message['mp3_fid'] = str(fs_mp3s.put(data))

    os.remove(mp3_file)

    try:
        channel.basic_publish(
            exchange="",
            routing_key=config.DB_MP3,
            body=json.dumps(message),
            properties=pika.BasicProperties(
                delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
            ),
        )
    except Exception as err:
        fs_mp3s.delete(message['mp3_fid'])
        return err


def callback(ch, method, properties, body):
    fs_videos = GridFS(config.client.videos)
    fs_mp3s = GridFS(config.client.mp3s)

    err = converter(ch, body, fs_videos, fs_mp3s)

    if err:
        print(f"{err}")
        ch.basic_nack(delivery_tag=method.delivery_tag)
    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)


def main():
    # rabbitMQ
    connection = queue.setup_connection()
    channel = queue.setup_channel(connection)

    print(
        f"Waiting to consume {config.DB_VIDEO} messages. To exit press CTRL+C")

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=config.DB_VIDEO, on_message_callback=callback)

    channel.start_consuming()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
