#!/usr/bin/env python3

from os import getenv
from pymongo import MongoClient

from dotenv import load_dotenv

load_dotenv()


# databases/queues
DB_MP3 = getenv("DB_MP3")
DB_VIDEO = getenv("DB_VIDEO")

# rabbitMQ
RABBITMQ_HOST = getenv("RABBITMQ_DEFAULT_HOST")
RABBITMQ_USER = getenv("RABBITMQ_DEFAULT_USER")
RABBITMQ_PASS = getenv("RABBITMQ_DEFAULT_PASS")

# mongoDB
MONGO_PORT = int(getenv("MONGO_PORT"))
MONGO_HOST = getenv("MONGO_HOST")

# configs
client = MongoClient(MONGO_HOST, MONGO_PORT)
