#!/usr/bin/env python3

import config
import os
import sys
import json
import smtplib

from email.message import EmailMessage
from utils import queue


def notification(message):
    try:
        message = json.loads(message)

        body = EmailMessage()
        body.set_content(f"""Hello {message['username']}!
MP3 Download https://mp3converter.com/{message['mp3_fid']}

Thank you for using our services.
Conveter Team""")
        body['Subject'] = f"mp3 file_id: {message['mp3_fid']} is now ready!"
        body['From'] = config.EMAIL_COMPANY
        body['To'] = message['email']

        session = smtplib.SMTP(config.SMTP_HOST, config.SMTP_PORT)
        session.starttls()
        session.login(config.EMAIL_ADDRESS, config.EMAIL_PASSWORD)
        session.send_message(body, config.EMAIL_ADDRESS, message['email'])

        print("Email sent")
    except (Exception) as err:
        return err


def callback(ch, method, properties, body):
    err = notification(body)

    if err:
        print(f"{err}")
        ch.basic_nack(delivery_tag=method.delivery_tag)
    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)


def main():
    # rabbitMQ
    connection = queue.setup_connection()
    channel = queue.setup_channel(connection)

    print(f"Waiting to consume {config.DB_MP3} messages. To exit press CTRL+C")

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=config.DB_MP3, on_message_callback=callback)

    channel.start_consuming()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
