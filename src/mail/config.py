#!/usr/bin/env python3

from os import getenv

from dotenv import load_dotenv

load_dotenv()


# databases/queues
DB_MP3 = getenv("DB_MP3")
DB_VIDEO = getenv("DB_VIDEO")

# rabbitMQ
RABBITMQ_HOST = getenv("RABBITMQ_DEFAULT_HOST")
RABBITMQ_USER = getenv("RABBITMQ_DEFAULT_USER")
RABBITMQ_PASS = getenv("RABBITMQ_DEFAULT_PASS")

# mongoDB
MONGO_PORT = int(getenv("MONGO_PORT"))
MONGO_HOST = getenv("MONGO_HOST")

# configs
SMTP_HOST = getenv("SMTP_HOST")
SMTP_PORT = int(getenv("SMTP_PORT"))

EMAIL_COMPANY = "video@converter.com"

EMAIL_ADDRESS = getenv("EMAIL_ADDRESS")
EMAIL_PASSWORD = getenv("EMAIL_PASSWORD")
