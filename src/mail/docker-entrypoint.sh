#!/bin/sh

echo "Waiting for RabbitMQ to start..."
sh wait-for.sh ${RABBITMQ_DEFAULT_HOST}:${RABBITMQ_DEFAULT_PORT}

echo "Starting the mail consumer..."
python3 notification.py
