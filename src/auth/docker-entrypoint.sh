#!/bin/sh

echo "Waiting for MySQL to start..."
sh wait-for.sh ${MYSQL_HOST}:${MYSQL_TCP_PORT}

echo "Starting the Flask server..."
flask run
