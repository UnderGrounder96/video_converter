#!/usr/bin/env python3

import config

from datetime import datetime

from flask import Flask, request

from flask_api import status
from flask_mysqldb import MySQL

from jwt import decode, encode
from passlib.hash import oracle10


app = Flask(__name__)
mysql = MySQL(app)

app.config.from_pyfile("config.py")


@app.route("/")
def index():
    return "Welcome", status.HTTP_200_OK


@app.route("/logout")
def logout():
    return "User was logged out", status.HTTP_200_OK


@app.route("/validate", methods=["POST"])
def validate():
    if "token" not in request.headers:
        return "Token is missing", status.HTTP_400_BAD_REQUEST

    try:
        return decode(
            request.headers['token'],
            config.JWT_SECRET,
            algorithms=[config.JWT_ALGO]
        ), status.HTTP_200_OK

    except (Exception):
        return "Invalid credentials provided", status.HTTP_400_BAD_REQUEST


@app.route("/login", methods=["POST"])
def login():
    auth_header = request.authorization
    auth_body = ["email", "password"]

    if not auth_header:
        return "Please provide digest authorization header", status.HTTP_400_BAD_REQUEST

    for field in auth_body:
        if field not in auth_header:
            return f"Please provide {field}", status.HTTP_400_BAD_REQUEST

    try:
        cur = mysql.connection.cursor()

        res = cur.execute(
            "SELECT username, email, password, role FROM users \
              WHERE active = 1 AND email = %(email)s",
            {'email': auth_header.email}
        )

        if res > 0:
            username, email, password, role = cur.fetchone()

            if oracle10.verify(auth_header.password, password, user=username):
                return create_token(username, role)

    except (Exception):
        return config.server_error, status.HTTP_500_INTERNAL_SERVER_ERROR

    return "Invalid credentials provided", status.HTTP_401_UNAUTHORIZED


@app.route("/register", methods=["POST"])
def register():
    auth_header = request.authorization
    auth_body = ["username", "email", "password", "role"]

    if not auth_header:
        return "Please provide digest authorization header", status.HTTP_400_BAD_REQUEST

    for field in auth_body:
        if field not in auth_header:
            return f"Please provide {field}", status.HTTP_400_BAD_REQUEST

    try:
        password = oracle10.hash(auth_header.password,
                                 user=auth_header.username)

    except (Exception):
        return config.server_error, status.HTTP_500_INTERNAL_SERVER_ERROR

    else:
        cur = mysql.connection.cursor()

        res = cur.execute(
            "SELECT username FROM users WHERE username = %(username)s",
            {'username': auth_header.username}
        )

        if res > 0:
            return config.server_error, status.HTTP_400_BAD_REQUEST

        elif res == 0:
            res = cur.execute(
                "INSERT INTO users(username, email, password, role, active) \
                  VALUES(%(username)s, %(email)s,%(password)s, %(role)s, 1)",
                {'username': auth_header.username, 'email': auth_header.email,
                    'password': password, 'role': auth_header.role}
            )

            if res > 0:
                return create_token(auth_header.username, auth_header.role)

    return config.server_error, status.HTTP_500_INTERNAL_SERVER_ERROR


def create_token(username, role):
    return encode(
        {
            "username": username,
            "exp": datetime.now() + config.JWT_LIFETIME,
            "iat": datetime.now(),
            "role": role,
        },
        config.JWT_SECRET,
        algorithm=config.JWT_ALGO,
    ), status.HTTP_200_OK
