#!/usr/bin/env python3

from os import getenv
from datetime import timedelta

from dotenv import load_dotenv

load_dotenv()


# configs
server_error = "Could not process input"

days = int(getenv("JWT_LIFETIME"))


JWT_ALGO = getenv("JWT_ALGO")
JWT_NAME = getenv("JWT_NAME")
JWT_SECRET = getenv("JWT_SECRET")
JWT_LIFETIME = timedelta(days=days)


MYSQL_HOST = getenv("MYSQL_HOST")
MYSQL_USER = getenv("MYSQL_USER")
MYSQL_PASSWORD = getenv("MYSQL_PASSWORD")
MYSQL_DB = getenv("MYSQL_DATABASE")
MYSQL_PORT = int(getenv("MYSQL_TCP_PORT"))
