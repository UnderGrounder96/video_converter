#!/usr/bin/env python3

import config

from gridfs import GridFS
from bson.objectid import ObjectId

from flask import Flask, request, session, redirect, url_for, send_file
from flask_api import status
from flask_pymongo import PyMongo
from flask_session import Session

from utils import auth, queue, storage

app = Flask(__name__)


mongo_mp3 = PyMongo(app, uri=config.MONGO_URI_MP3)
mongo_video = PyMongo(app, uri=config.MONGO_URI_VIDEO)

fs_videos = GridFS(mongo_video.db)
fs_mp3s = GridFS(mongo_mp3.db)


# rabbitMQ
connection = queue.setup_connection()
channel = queue.setup_channel(connection)


app.config.from_pyfile("config.py")

Session(app)


@app.route("/")
def index():
    if "username" in session:
        return f"Logged in as {session['username']}", status.HTTP_200_OK

    return "Please log in", status.HTTP_401_UNAUTHORIZED


@app.route("/logout")
def logout():
    session.clear()

    return "User was logout", status.HTTP_200_OK


@app.route("/login", methods=["POST"])
def login():
    if "username" in session:
        return redirect(url_for("index"))

    return auth.auth_check(request, session, "login")


@app.route("/register", methods=["POST"])
def register():
    if "username" in session:
        return redirect(url_for("index"))

    return auth.auth_check(request, session, "register")


@app.route("/upload", methods=["POST"])
def upload():
    if "token" not in session:
        return redirect(url_for("index"))

    err = auth.validate_token(session['token'])

    if err:
        return err

    files = request.files

    if len(files) != 1:
        return "Video file required", status.HTTP_400_BAD_REQUEST

    # the input nave must be video
    err = storage.upload(files['video'], fs_videos, channel, app.logger.info)

    if err:
        return err

    return "Upload successfully", status.HTTP_200_OK


@app.route("/download", methods=["POST"])
def download():
    if "token" not in session:
        return redirect(url_for("index"))

    err = auth.validate_token(session['token'])

    if err:
        return err

    fid_string = request.args.get("fid")

    if not fid_string:
        return "fid query argument required", status.HTTP_400_BAD_REQUEST

    try:
        out = fs_mp3s.get(ObjectId(fid_string))
        return send_file(out, download_name=f"{fid_string}.mp3")
    except (Exception) as err:
        app.logger.info(err)
        return config.server_error, status.HTTP_500_INTERNAL_SERVER_ERROR
