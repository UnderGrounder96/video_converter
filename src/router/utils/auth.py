#!/usr/bin/env python3

import config

from requests import post

from flask_api import status


def auth_connect(path, headers):
    auth_route = f"http://{config.AUTH_ADDRESS}/{path}"

    return post(auth_route, headers=headers)


def auth_check(request, session, path):
    headers = request.headers
    response = auth_connect(path, headers)

    if response.status_code == status.HTTP_200_OK:
        session['token'] = response.text
        session['role'] = request.authorization.role
        session['email'] = request.authorization.email
        session['username'] = request.authorization.username

        return response.text, None

    return response.text, response.status_code


def validate_token(token):
    auth_route = f"http://{config.AUTH_ADDRESS}/validate"

    response = post(auth_route, headers={'token': token})

    if response.status_code == status.HTTP_200_OK:
        return None

    return response.text, response.status_code
