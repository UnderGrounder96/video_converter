#!/usr/bin/env python3

import config
import json
import pika

from flask import session
from flask_api import status


def allowed_format(filename):
    return filename.rsplit('.', 1)[1].lower() in config.ALLOWED_FILE_FORMATS


def upload(video, fs_videos, channel, print):
    if not allowed_format(video.filename):
        return "Wrong file format provided", status.HTTP_400_BAD_REQUEST

    try:
        fid = fs_videos.put(video)
    except (Exception) as err:
        print(err)
        return config.server_error, status.HTTP_500_INTERNAL_SERVER_ERROR

    message = {
        "video_fid": str(fid),
        "mp3_fid": None,
        "username":  session['username'],
        "email":  session['email'],
    }

    try:
        channel.basic_publish(
            exchange="",
            routing_key="videos",
            body=json.dumps(message),
            properties=pika.BasicProperties(
                delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
            )
        )
    except (Exception) as err:
        print(err)
        fs_videos.delete(fid)
        return config.server_error, status.HTTP_500_INTERNAL_SERVER_ERROR
