#!/usr/bin/env python3

import config

from pika import PlainCredentials, ConnectionParameters, BlockingConnection


# rabbitMQ
def setup_connection():
    credentials = PlainCredentials(
        config.RABBITMQ_USER, config.RABBITMQ_PASS)

    parameters = ConnectionParameters(
        host=config.RABBITMQ_HOST, credentials=credentials)

    return BlockingConnection(parameters)


def setup_channel(connection):
    channel = connection.channel()

    channel.queue_declare(queue=config.DB_MP3, durable=True)
    channel.queue_declare(queue=config.DB_VIDEO, durable=True)

    return channel
