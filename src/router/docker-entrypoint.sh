#!/bin/sh

echo "Waiting for MongoDB to start..."
sh wait-for.sh ${MONGO_HOST}:${MONGO_PORT}

echo "Waiting for RabbitMQ to start..."
sh wait-for.sh ${RABBITMQ_DEFAULT_HOST}:${RABBITMQ_DEFAULT_PORT}

echo "Waiting for Redis to start..."
sh wait-for.sh ${REDIS_HOST}:${REDIS_PORT}

echo "Starting the Flask server..."
flask run
