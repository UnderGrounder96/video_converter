#!/usr/bin/env python3


import redis

from os import getenv
from datetime import timedelta

from dotenv import load_dotenv

load_dotenv()


# configs
server_error = "Could not process input"

days = int(getenv("SESSION_LIFETIME"))

SECRET_KEY = getenv("SECRET_KEY")
SESSION_COOKIE_NAME = getenv("SESSION_NAME")
PERMANENT_SESSION_LIFETIME = timedelta(days=days)

SESSION_TYPE = "redis"
SESSION_REDIS = redis.Redis(
    host=getenv("REDIS_HOST"),
    port=int(getenv("REDIS_PORT"))
)

ALLOWED_FILE_FORMATS = {"3gp", "avi", "mpg-4", "mp4", "mkv"}
MAX_CONTENT_LENGTH = 16 * 1000_000  # 16MB upload limit

# auth
AUTH_ADDRESS = f"{getenv('AUTH_HOST')}:{int(getenv('AUTH_PORT'))}"


# databases/queues
DB_MP3 = getenv("DB_MP3")
DB_VIDEO = getenv("DB_VIDEO")


# rabbitMQ
RABBITMQ_HOST = getenv("RABBITMQ_DEFAULT_HOST")
RABBITMQ_USER = getenv("RABBITMQ_DEFAULT_USER")
RABBITMQ_PASS = getenv("RABBITMQ_DEFAULT_PASS")


# mongoDB
MONGO_PORT = int(getenv("MONGO_PORT"))
MONGO_HOST = getenv("MONGO_HOST")
MONGO_URI = f"mongodb://{MONGO_HOST}:{MONGO_PORT}"

MONGO_URI_MP3 = f"{MONGO_URI}/{DB_MP3}"
MONGO_URI_VIDEO = f"{MONGO_URI}/{DB_VIDEO}"
