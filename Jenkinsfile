#!/usr/bin/env groovy


def vars, FAILED_STAGE


pipeline{
    agent any

    options {
        timestamps()
        disableConcurrentBuilds()
    }


    stages {
        stage("Load Config") {
            steps {
                echo "========Executing ${env.STAGE_NAME}========"

                script {
                  vars = load pwd() + "/ci/vars.groovy"
                }
            }
        }

        stage("Build") {
            steps {
                echo "========Executing ${env.STAGE_NAME}========"

                // AUTH -----------------------------------------
                echo "Building Auth images"

                sh "docker build -t ${vars.img_auth} ${vars.src_auth}"
                sh "docker tag ${vars.img_auth} ${vars.img_auth}:${env.BUILD_NUMBER}"

                sh "docker build -t ${vars.img_auth_db} -f ${vars.df_auth_db} ${vars.src_auth}"
                sh "docker tag ${vars.img_auth_db} ${vars.img_auth_db}:${env.BUILD_NUMBER}"
                // END AUTH -------------------------------------

                // ROUTER -----------------------------------------
                echo "Building Router image"

                sh "docker build -t ${vars.img_router} ${vars.src_router}"
                sh "docker tag ${vars.img_router} ${vars.img_router}:${env.BUILD_NUMBER}"
                // END ROUTER -------------------------------------

                // MAIL -----------------------------------------
                echo "Building Mail image"

                sh "docker build -t ${vars.img_mail} ${vars.src_mail}"
                sh "docker tag ${vars.img_mail} ${vars.img_mail}:${env.BUILD_NUMBER}"
                // END MAIL -------------------------------------

                // RESOLVER -----------------------------------------
                echo "Building Resolver image"

                sh "docker build -t ${vars.img_resolver} ${vars.src_resolver}"
                sh "docker tag ${vars.img_resolver} ${vars.img_resolver}:${env.BUILD_NUMBER}"
                // END RESOLVER -------------------------------------
            }
        }

        stage("Publish") {
            steps {
                echo "========Executing ${env.STAGE_NAME}========"

                // AUTH -----------------------------------------
                echo "Publishing Auth images"

                sh "kind load docker-image ${vars.img_auth}"
                sh "kind load docker-image ${vars.img_auth_db}"
                // END AUTH -------------------------------------

                // ROUTER -----------------------------------------
                echo "Publishing Router image"

                sh "kind load docker-image ${vars.img_router}"
                // END ROUTER -------------------------------------

                // MAIL -----------------------------------------
                echo "Publishing Mail image"

                sh "kind load docker-image ${vars.img_mail}"
                // END MAIL -------------------------------------

                // RESOLVER -----------------------------------------
                echo "Publishing Resolver image"

                sh "kind load docker-image ${vars.img_resolver}"
                // END RESOLVER -------------------------------------
            }
        }

        stage("Deploy") {
            steps {
                echo "========Executing ${env.STAGE_NAME}========"

                // AUTH -----------------------------------------
                echo "Deploying Auth"

                sh "kubectl apply -f ${vars.src_auth_k8s}"
                // END AUTH -------------------------------------

                // MONGO -----------------------------------------
                echo "Deploying Mongo"

                sh "kubectl apply -f ${vars.src_mongo_k8s}"
                // END MONGO -------------------------------------

                // QUEUE -----------------------------------------
                echo "Deploying Queue"

                sh "kubectl apply -f ${vars.src_queue_k8s}"
                // END QUEUE -------------------------------------

                // MAIL -----------------------------------------
                echo "Deploying Mail"

                sh "kubectl apply -f ${vars.src_mail_k8s}"
                // END MAIL -------------------------------------

                // RESOLVER -----------------------------------------
                echo "Deploying Resolver"

                sh "kubectl apply -f ${vars.src_resolver_k8s}"
                // END RESOLVER -------------------------------------

                // REDIS -----------------------------------------
                echo "Deploying Redis"

                sh "kubectl apply -f ${vars.src_redis_k8s}"
                // END REDIS -------------------------------------

                // ROUTER -----------------------------------------
                echo "Deploying Router"

                sh "kubectl apply -f ${vars.src_router_k8s}"
                // END ROUTER -------------------------------------
            }
        }
    }


    post {
        always {
            echo "========always========"
        }

        success {
            echo "========pipeline executed successfully========"
        }

        failure {
            echo "========pipeline execution failed========"

            echo "Failed stage name: ${FAILED_STAGE}"
        }
    }
}
